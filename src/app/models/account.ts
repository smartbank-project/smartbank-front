import { Transaction } from './transaction';

export interface Account{

id: number;
balance: number;
overdraft: number;
transactions: Transaction[];

}
