export interface Transaction{
id: number;
transactionType: string;
amount: number;
transactionTime: Date ;
}
