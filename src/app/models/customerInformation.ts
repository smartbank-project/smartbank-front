import { Transaction } from './transaction';
export interface CustomerInformation{

    name: string;
    surname: string;
    balance: number;
    transactions: Transaction[];

}
