export interface NewAccountRequest{
  customerId: string;
  initialCredit: number;
}
