import { Account } from './account';

export interface Customer{
id: number;
customerId: string;
name: string;
surname: string;
account: Account;
}
