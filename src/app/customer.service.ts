import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators'
import { Customer } from './models/customer';
import { NewAccountRequest } from './models/newAccountRequest';
import { CustomerInformation } from './models/customerInformation';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

endPointAllCustomerWithoutAccount = 'http://localhost:8080/customers';
endPointAllCustomerHavingAccount = 'http://localhost:8080/customers/active';
endPointCustomerInfos = 'http://localhost:8080/customer';

  constructor(private readonly httpClient: HttpClient) { }

// Return all customers with NO account
public getCustomers(): Observable<Customer[]>{
      return this.httpClient.get<Customer[]>(this.endPointAllCustomerWithoutAccount).pipe(shareReplay(1));
}

// Return all customers having account
public getCustomersWithAccount(): Observable<Customer[]>{
      return this.httpClient.get<Customer[]>(this.endPointAllCustomerHavingAccount).pipe(shareReplay(1));
}

//Create new account for a specific customer
public createAccount(newAccount: NewAccountRequest): Observable<any>{
 return this.httpClient.post('http://localhost:8080/new-account', newAccount)
  .pipe(shareReplay(1));
}

//Get information about a user
public getCustomerInforations(id: number): Observable<CustomerInformation>{
      return this.httpClient.get<CustomerInformation>(this.endPointCustomerInfos + '/' + id as string).pipe(shareReplay(1));
}
}
