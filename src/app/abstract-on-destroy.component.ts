import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export abstract class AbstractOnDestroyComponent implements OnDestroy {

 protected unsubscribe: Subject<any> = new Subject();

  ngOnDestroy(): void {
  this.unsubscribe.next('');
  this.unsubscribe.complete();
  }

}
