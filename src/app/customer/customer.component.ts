import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { CustomerService } from '../customer.service';
import { takeUntil } from 'rxjs/operators';
import { AbstractOnDestroyComponent } from '../abstract-on-destroy.component';
import { Customer } from '../models/customer';
import { NewAccountRequest } from '../models/newAccountRequest';
import { CustomerInformation } from '../models/customerInformation';



@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent extends AbstractOnDestroyComponent implements OnInit {

  form: FormGroup;
  customers: Customer[] = [];
  customerWithAccount: Customer[] =  [];
  newAccount: NewAccountRequest = {customerId: '0', initialCredit: 0.0};
  customerToShowDetail: CustomerInformation = {name: '', surname: '', balance: 0.0, transactions: []};

  constructor(private readonly customerService: CustomerService, public fb: FormBuilder) {
  super();
  this.form = this.fb.group({
  customerId: [null],
  initialCredit: [0.0]
  });
  }

  ngOnInit(): void{
  this.getCustomers();
  this.getCustomersWithAccount();
  this.initForm();
  }

  getCustomers(): void {
       this.customerService.getCustomers().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
       this.customers = data as Customer[];
       });
  }
  getCustomersWithAccount(): void {
    this.customerService.getCustomersWithAccount().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
       this.customerWithAccount = data as Customer[];
       });
  }
private initForm(): void{
  this.form = new FormGroup({
    customerId: new FormControl(null),
    initialCredit: new FormControl(0.0)
  });
}


  public createCurrentAccount(): void{
    this.newAccount.customerId = this.form.get('customerId')?.value;
    this.newAccount.initialCredit = this.form.get('initialCredit')?.value;
    this.customerService.createAccount(this.newAccount).pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
       // console.log({data})
        });
        window.location.reload();
  }

//Show customer details
public showCustomerDetails(id: number): void{
  this.customerService.getCustomerInforations(id).pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
  this.customerToShowDetail = data;
  });
}
}
